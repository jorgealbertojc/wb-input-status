


(function () {
    'use strict';



    function spinner () {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: '/views/directives/spinner.tpl.html',
            scope: {
                show: '='
            }
        };
    }



    angular.module( 'webapp' )
        .directive( 'spinner', spinner );
}());