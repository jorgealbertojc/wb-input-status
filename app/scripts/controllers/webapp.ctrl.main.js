


(function () {
    'use strict';



    function MainCtrl ( $scope, $timeout ) {



        function setup () {
            $scope.$$names = /^([a-zA-Z\sÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð])+$/;
            $scope.$$zipCode = /^([0-9]{5})+$/;
            $scope.field1Status = null;
            $scope.field2Status = null;
            $scope.titularMiddleName = '12345';
            $scope.data = {
                titularName: null,
                titularMiddleName: null
            };
        }



        $scope.validateTitularName = function () {
            console.log( '$scope.titularData.titularName.$valid', $scope.titularData.titularName.$valid );
            if( ! $scope.titularData.titularName.$valid ) {
                $scope.field1Status = 'ERROR';
            } else {
                $scope.field1Status = 'SUCCESS';
            }
        };



        $scope.validateTitularMiddleName = function () {
            console.log( '$scope.titularData.titularMiddleName.$valid', $scope.titularData.titularMiddleName.$valid );
            $scope.field2Status = 'LOADING';
            $timeout( function () {
                if( ! $scope.titularData.titularMiddleName.$valid ) {
                    $scope.field2Status = 'ERROR';
                } else {
                    $scope.field2Status = 'SUCCESS';
                }
            }, 3000 );
        };



        setup();
    }



    angular.module( 'webapp' )
        .controller( 'webapp.ctrl.main', MainCtrl );
}());
