


(function () {
    'use strict';



    module.exports = function ( grunt ) {



        require( 'load-grunt-tasks' )( grunt );

        require ( './grunt-config-tasks/grunt-config-tasks.js' )( grunt );


        grunt.registerTask( 'compile', [
            'clean',
            'jshint',
            'copy',
            'ngAnnotate',
            'uglify',
            'compass',
            'autoprefixer',
            'jade'
        ] );

        grunt.registerTask( 'default', [
            'compile',
            'connect',
            'watch'
        ] );
    };
}());
