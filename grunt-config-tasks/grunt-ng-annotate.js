


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var ngAnnotate = {};

        function getFilesObject () {
            return [
                {
                    expand: true,
                    cwd: 'js',
                    src: [
                        '**/*.js',
                        '!**/components/**',
                        '!**/bower_components/**',
                    ],
                    dest: 'js'
                }
            ];
        }

        ngAnnotate.all = {
            files: getFilesObject()
        };



        return {
            name: 'ngAnnotate',
            config: ngAnnotate
        };
    };
}());
