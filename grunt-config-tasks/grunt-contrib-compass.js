


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var compass = {};



        compass.all = {
            options: {
                outputStyle: 'compressed',
                sassDir: 'app/styles',
                cssDir: 'css'
            }
        };



        return {
            name: 'compass',
            config: compass
        };
    };
}());
