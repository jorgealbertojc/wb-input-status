


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var copy = {};



        copy.all = {
            files: [
                {
                    expand: true,
                    cwd: 'app/images',
                    src: [
                        '**/*.jpg',
                        '**/*.png',
                        '**/*.svg',
                        '**/*.gif',
                        '**/*.jpeg'
                    ],
                    dest: 'img'
                },
                {
                    expand: true,
                    cwd: 'app/scripts',
                    src: [
                        '**/*.js'
                    ],
                    dest: 'js'
                }
            ]
        };


        return {
            name: 'copy',
            config: copy
        };
    };
}());
