


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var cssmin = {};



        cssmin.all = {
            files: [
                {
                    expand: true,
                    cwd: 'css',
                    src: [
                        '**/*.css',
                        '!**/components/**',
                        '!**/bower_components/**'
                    ],
                    dest: 'css',
                    ext: '.min.css'
                }
            ]
        };



        return {
            name: 'cssmin',
            config: cssmin
        };
    };
}());
