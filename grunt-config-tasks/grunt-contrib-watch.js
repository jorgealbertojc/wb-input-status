


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var watch = {};



        watch.scripts = {
            files: [
                'app/scripts/**/*.js'
            ],
            tasks: [
                'jshint',
                'copy',
                'ngAnnotate'
            ],
            options:  {
                livereload: 35728
            }
        };



        watch.styles = {
            files: [
                'app/styles/**/*.scss',
            ],
            tasks: [
                'compass',
                'autoprefixer',
                'cssmin'
            ],
            options:  {
                livereload: 35728
            }
        };



        watch.images = {
            files: [
                'app/images/**/*',
            ],
            tasks: [
                'copy'
            ],
            options:  {
                livereload: 35728
            }
        };



        watch.jade_elements = {
            files: [
                'app/jade/**/*.jade',
            ],
            tasks: [
                'jade'
            ],
            options:  {
                livereload: 35728
            }
        };



        return {
            name: 'watch',
            config: watch
        };
    };
}());
