


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var connect = {};



        connect.server = {
            options: {
                hostname: '0.0.0.0',
                port: 9001,
                base: './',
                open: {
                    target: 'http://localhost:9001'
                }
            }
        };



        return {
            name: 'connect',
            config: connect
        };
    };
}());
