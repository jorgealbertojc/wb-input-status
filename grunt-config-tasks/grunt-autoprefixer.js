


(function () {
    'use strict';



    module.exports = function ( _grunt ) {

        var autoprefixer = {};



        autoprefixer.all = {
            files: [
                {
                    expand: true,
                    cwd: 'css',
                    src: [
                        '**/*.css',
                        '!**/components/**',
                        '!**/bower_components/**'
                    ],
                    dest: 'css',
                }
            ]
        };



        return {
            name: 'autoprefixer',
            config: autoprefixer
        };
    };
}());
