


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var jshint = {};

        jshint.options = {
            jshintrc: './.jshintrc'
        };

        jshint.public = [
            'app/scripts'
        ];



        return {
            name: 'jshint',
            config: jshint
        };
    };
}());
