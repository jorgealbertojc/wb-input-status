


(function () {



    var fs = require( 'fs' );
    var _ = require( 'underscore' );



    module.exports = function ( _grunt ) {



        var config_tasks_dir = './grunt-config-tasks';
        var config_tasks_file = 'grunt-config-tasks.js';
        var loaded_file_contents = null;
        var executed_config = null;
        var init_grunt_config = {};
        var list_files = fs.readdirSync( config_tasks_dir );



        function getFileWithOutext ( _fileName ) {
            return _fileName.substring( 0, _fileName.length - 3 );
        }



        _.each( list_files , function ( _file_name ) {

             if ( _file_name !== config_tasks_file ) {

                loaded_file_contents = require( './' + getFileWithOutext( _file_name ) );

                if ( typeof loaded_file_contents === 'function' ) {

                    executed_config = loaded_file_contents( _grunt );
                    init_grunt_config [ executed_config.name ] = executed_config.config;
                }
            }
        });



        _grunt.initConfig( init_grunt_config );

    };
}());
