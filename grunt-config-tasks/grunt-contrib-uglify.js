


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var uglify = {};



        function getFilesObject () {
            return [
                {
                    expand: true,
                    cwd: 'js',
                    src: [
                        '**/*.js'
                    ],
                    dest: 'js'
                }
            ];
        }



        uglify.public = {
            files: getFilesObject()
        };



        return {
            name: 'uglify',
            config: uglify
        };
    };
}());
