


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var jade = {};



        jade.all = {
            files: [
                {
                    expand: true,
                    cwd: 'app/jade',
                    src: [
                        '**/*.jade',
                        '!**/includes/**',
                        '!**/mixins/**',
                        '!**/layouts/**'
                    ],
                    dest: './',
                    extDot: 'last',
                    ext: '.html'
                }
            ]
        };



        return {
            name: 'jade',
            config: jade
        };
    };
}());
