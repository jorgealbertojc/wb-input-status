


(function () {
    'use strict';



    module.exports = function ( _grunt ) {



        var clean = {};



        clean.options = {
            force: true
        };



        clean.all = [
            'views',
            'index.html',
            'js',
            'css',
            'img'
        ];



        return {
            name: 'clean',
            config: clean
        };
    };
}());
